package pl.me.jruleengine;

import android.Manifest;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.engine.efficiency.java.rmi.RemoteException;

import org.jruleengine.Clause;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.rules.ConfigurationException;
import javax.rules.InvalidRuleSessionException;
import javax.rules.RuleExecutionSetNotFoundException;
import javax.rules.RuleRuntime;
import javax.rules.RuleServiceProvider;
import javax.rules.RuleServiceProviderManager;
import javax.rules.RuleSessionCreateException;
import javax.rules.RuleSessionTypeUnsupportedException;
import javax.rules.StatefulRuleSession;
import javax.rules.admin.RuleAdministrator;
import javax.rules.admin.RuleExecutionSet;
import javax.rules.admin.RuleExecutionSetCreateException;
import javax.rules.admin.RuleExecutionSetRegisterException;

import engine.ocenakandydata;


public class MainActivity extends Activity {
    Button button;
    TextView textView;

    private static List<Long> memoryUsage = new LinkedList<>();
    private static List<Float> processorUsage = new LinkedList<>();
    private static boolean ifContinue = true;
    private Date start, end;
    private BufferedWriter bw;
    private final Runtime runtime = Runtime.getRuntime();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button2);
        textView = (TextView) findViewById(R.id.textView2);
        textView.setMovementMethod(new ScrollingMovementMethod());
//        new Monitor().execute();
        int permissionCheck;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            permissionCheck = this.checkSelfPermission("Manifest.permission.READ_EXTERNAL_STORAGE");
            if (permissionCheck != 0) {
                this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1001); //Any number
            }
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try {
//                    bw = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory()
//                            .getAbsolutePath()+"/benchmark/jruleengine.txt",true));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                //start = new Date();
                dzialaj2(v);
                //end = new Date();
                //ifContinue = false;
            }
        });
    }

    public void dzialaj(View view) {
        try {
            Class.forName( "org.jruleengine.RuleServiceProviderImpl" );
            String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/example3.xml";
            InputStream inStream = new FileInputStream( new File(path) );
            // Get the rule service provider from the provider manager.
            RuleServiceProvider serviceProvider;
            serviceProvider = RuleServiceProviderManager.getRuleServiceProvider("org.jruleengine");
            // get the RuleAdministrator
            RuleAdministrator ruleAdministrator = serviceProvider.getRuleAdministrator();
//            textView.setText(textView.getText().toString() + "\n\nDEBUG: " + "Administration API");
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Acquired RuleAdministrator: " + ruleAdministrator);
            // get an input stream to a test XML ruleset
            // This rule execution set is part of the TCK.
            // InputStream inStream = new FileInputStream( "example3.xml" );
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Acquired InputStream to example3.xml: " + inStream);
            // parse the ruleset from the XML document
            RuleExecutionSet res1 = ruleAdministrator.getLocalRuleExecutionSetProvider( null ).createRuleExecutionSet(inStream, null);
            inStream.close();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Loaded RuleExecutionSet: " + res1);
            // register the RuleExecutionSet
            String uri = res1.getName();
            ruleAdministrator.registerRuleExecutionSet(uri, res1, null );
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Bound RuleExecutionSet to URI: " + uri);
            // Get a RuleRuntime and invoke the rule engine.
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "\nRuntime API\n");
            RuleRuntime ruleRuntime = serviceProvider.getRuleRuntime();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Acquired RuleRuntime: " + ruleRuntime);
            // create a StatefulRuleSession
            StatefulRuleSession statefulRuleSession = (StatefulRuleSession) ruleRuntime.createRuleSession( uri, new HashMap(), RuleRuntime.STATEFUL_SESSION_TYPE );
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Got Stateful Rule Session: " + statefulRuleSession);
            // Add some clauses...
            Customer c = new Customer();
            c.setCreditLimit(370.0);
            Invoice i = new Invoice();
            i.setAmount(200.0);
            statefulRuleSession.addObject(c);
            statefulRuleSession.addObject(i);
            ArrayList<Clause> input = new ArrayList<>();
            input.add(new Clause("Socrate is human"));
            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Called executeRules");
            // extract the Objects from the statefulRuleSession
            // add an Object to the statefulRuleSession
            statefulRuleSession.addObjects( input );
            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Called addObject on Stateful Rule Session: " + statefulRuleSession);
            statefulRuleSession.executeRules();
            List results = statefulRuleSession.getObjects();
            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Result of calling getObjects: " + results.size() + " results.");
            // Loop over the results.
            for (Object obj : results) {
                textView.setText(textView.getText().toString() + "\nDEBUG: " + "Clause Found: " + obj.toString());
            }
            // release the statefulRuleSession
            statefulRuleSession.release();
            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Released Stateful Rule Session.");

        } catch (ConfigurationException | ClassNotFoundException | FileNotFoundException | RemoteException | RuleExecutionSetCreateException | RuleExecutionSetRegisterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuleSessionCreateException e) {
            e.printStackTrace();
        } catch (RuleExecutionSetNotFoundException e) {
            e.printStackTrace();
        } catch (RuleSessionTypeUnsupportedException e) {
            e.printStackTrace();
        } catch (InvalidRuleSessionException e) {
            e.printStackTrace();
        }
    }

    public void dzialaj2(View view) {
        try {
            Class.forName( "org.jruleengine.RuleServiceProviderImpl" );
            String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/jruleengine/psc-zatrudnienie-ocena_kandydata.pl.xml";
            InputStream inStream = new FileInputStream( new File(path) );
            // Get the rule service provider from the provider manager.
            RuleServiceProvider serviceProvider;
            serviceProvider = RuleServiceProviderManager.getRuleServiceProvider("org.jruleengine");
            // get the RuleAdministrator
            RuleAdministrator ruleAdministrator = serviceProvider.getRuleAdministrator();
//            textView.setText(textView.getText().toString() + "\n\nDEBUG: " + "Administration API");
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Acquired RuleAdministrator: " + ruleAdministrator);
            // get an input stream to a test XML ruleset
            // This rule execution set is part of the TCK.
            // InputStream inStream = new FileInputStream( "example3.xml" );
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Acquired InputStream to "+path+": " + inStream);
            // parse the ruleset from the XML document
            RuleExecutionSet res1 = ruleAdministrator.getLocalRuleExecutionSetProvider( null ).createRuleExecutionSet(inStream, null);
            inStream.close();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Loaded RuleExecutionSet: " + res1);
            // register the RuleExecutionSet
            String uri = res1.getName();
            ruleAdministrator.registerRuleExecutionSet(uri, res1, null );
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Bound RuleExecutionSet to URI: " + uri);
            // Get a RuleRuntime and invoke the rule engine.
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "\nRuntime API\n");
            RuleRuntime ruleRuntime = serviceProvider.getRuleRuntime();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Acquired RuleRuntime: " + ruleRuntime);
            // create a StatefulRuleSession
            StatefulRuleSession statefulRuleSession = (StatefulRuleSession) ruleRuntime.createRuleSession( uri, new HashMap(), RuleRuntime.STATEFUL_SESSION_TYPE );
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Got Stateful Rule Session: " + statefulRuleSession);
            // Add some clauses...

            ocenakandydata _ocenakandydata = new ocenakandydata();
            _ocenakandydata.setOcenaRozmowy(2.0);
            _ocenakandydata.setOcenaKwalifikacji(5.0);
            _ocenakandydata.setOcenaTestow(2.0);
            statefulRuleSession.addObject(_ocenakandydata);
            // add an Object to the statefulRuleSession
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Called addObject on Stateful Rule Session: " + statefulRuleSession);
            start = new Date();
            statefulRuleSession.executeRules();
            end = new Date();
            ifContinue = false;
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Called executeRules");
            // extract the Objects from the statefulRuleSession
            List results = statefulRuleSession.getObjects();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Result of calling getObjects: " + results.size() + " results.");
            // Loop over the results.
            for (Object obj : results) {
                textView.setText(textView.getText().toString() + "\nDEBUG: " + "Clause Found: " + obj.toString());
            }
            // release the statefulRuleSession
            statefulRuleSession.release();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Released Stateful Rule Session.");

        } catch (ConfigurationException | ClassNotFoundException | FileNotFoundException | RemoteException | RuleExecutionSetCreateException | RuleExecutionSetRegisterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuleSessionCreateException e) {
            e.printStackTrace();
        } catch (RuleExecutionSetNotFoundException e) {
            e.printStackTrace();
        } catch (RuleSessionTypeUnsupportedException e) {
            e.printStackTrace();
        } catch (InvalidRuleSessionException e) {
            e.printStackTrace();
        }
    }

    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception ignored) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public class Monitor extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (ifContinue) {
                memoryUsage.add(runtime.totalMemory() - runtime.freeMemory());
                processorUsage.add(readUsage());
            }
            System.out.println("MONITOR: KONIEC");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Float [] proc = processorUsage.toArray(new Float[processorUsage.size()]);
            float minProc = proc[0];
            float maxProc = proc[0];
            for (float p : proc) {
                if (p < minProc) minProc = p;
                if (p > maxProc) maxProc = p;
            }
            try {
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB, Processor diff: ");
                bw.write(String.valueOf(maxProc-minProc));
                bw.write(" %, Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms\n-------\n");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
