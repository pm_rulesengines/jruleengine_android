# README #

This README contains specific information for JRuleEngine.

### Essential files ###

Before launching the app, you need to upload an XML model file to the phone memory. The file has to be placed in the internal storage inside "jruleengine" directory. The attributes used in the model have also to be placed in the code (the class representing used attribute is already in the project code). A ZIP archive with XML file and model package is available in the "Downloads" section.